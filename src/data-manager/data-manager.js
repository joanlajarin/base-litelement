import { LitElement, html } from 'lit-element';

class DataManager extends LitElement{

    static get properties() {
        return {
            people: {type: Array}
        };
    }
    constructor() {
        super();

        this.people = [
            {
                name: "Elle Ripley",
                yearsInCompany : 10,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley" //si no se puede cargar la imagen aparece este valor
                },
                profile: "Lore ipsum dolor sit amet"
            }, {
                name: "Bruce Banner",
                yearsInCompany : 2,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner" 
                },
                profile: "Lore dolor ipsum sit amet"
            }, {
                name: "Eowyn",
                yearsInCompany : 5,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn"
                },
                profile: "Dolor lore ipsum  sit amet"
            }, {
                name: "Turanga Leela",
                yearsInCompany : 9,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn" //si no se puede cargar la imagen aparece este valor
                },
                profile: "Lore ipsum sit amet dolor"
            }, {
                name: "Tyrion Lannister",
                yearsInCompany : 1,
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn" //si no se puede cargar la imagen aparece este valor
                },
                profile: "Lore ipsum sit amet dolor sit amet"      
            }
        ]    
    }

    updated(changedProperties) {
        console.log("updated");

        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people")

            this.dispatchEvent(
                new CustomEvent(
                    "people-data-updated",
                    {
                        detail : {
                            people : this.people
                        }
                    }
                )
            )
        }
    }
}

customElements.define('data-manager', DataManager);